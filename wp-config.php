<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'advocate' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'OAO_xL$wFj*^wn Pv<4VZ#*]oq9.Mfzq;P7!)IX.;UN)X!h}q&QL8Da0l`l,nICu' );
define( 'SECURE_AUTH_KEY',  'Vk:L1:<Rtj,td{ITVP&cr;=|-/VM.ZCCb$qXO&DaW03k#c_Af&R_Na-;@>XbTiZz' );
define( 'LOGGED_IN_KEY',    '`q40?wksF$3AYRiRw2J_^H@{~ 7S-[@p&``~/~r)fpMfbxl>UiLCm>eO6h,qvp5J' );
define( 'NONCE_KEY',        'vu(zHKrz=o`EA-(_4QQ3xB4JvY<3e.R0naRz=xC^}4@^[Gq, 25>[;97[M]} e0M' );
define( 'AUTH_SALT',        'IOR*@qRngxPq}#yJd+}yTw+~&x_Z4gZMb>#q@m<f~0c;z2~C/`[#siU)3]-/s;fT' );
define( 'SECURE_AUTH_SALT', 'eDZ]jc!g3`*_x?%`1?A8c9h%?r weAh*NA 831l,C^;67PK N[ammyrXBVL AYU_' );
define( 'LOGGED_IN_SALT',   '69HKx?XQ{@p2=w]HP/CfQOE&x4(Ml>5Jw2]E2+Hq6Z.+|b+h mK_:(|>Tt=vYS=|' );
define( 'NONCE_SALT',       'Uz9]7rR7@/ZuJ(].h>q54&-%2p9jE|qmo4Xaw}anO3;$Oa|N{:|F6P^Y[2LfdbJy' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'adv_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
